


function first($scope){

    if(Lockr.get('notFirst') == true)
    	return false;

    // Inicializa os blocos no Disco.
	Lockr.set('blocos', {});
	Lockr.set('ordem', []);

	//Busca a linguagem do usuário.
	document.addEventListener("deviceready", () => {
		navigator.globalization.getPreferredLanguage(
			function (language) {$scope.defineIdioma(language.value);$scope.$digest();},
			function () {$scope.defineIdioma('en-US');$scope.$digest();}
		);
	}, false);
	

	// Define que não é mais o primeiro acesso.
	Lockr.set('notFirst', true);
	
	return true;
}