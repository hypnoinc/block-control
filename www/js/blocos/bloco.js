
var scope = null;

class Bloco{

    static buscar(){
        var objs = Lockr.get('blocos');
        for (var i in objs){
            var tipo = scope.lang.tipo[objs[i].tipo].sigla;
            var classe = 'Bloco'+tipo.charAt(0).toUpperCase() + tipo.slice(1);
            objs[i] = new window[classe](objs[i]); 
        }
        return objs;
    }

    static setScope($scope){
        scope = $scope;
    }

    static getScope(){
        return scope;
    }

    static inserir(bloco){
        if(!(bloco instanceof Bloco))
            throw 'objeto recebido não é um Bloco';
        var last = Number(Object.keys(scope.blocos).pop());
        var id = isNaN(last) ? 0 : last + 1;
        scope.blocos[id] = bloco;
        scope.ordem.push(id);
        scope.countBlocos++;
        Lockr.set('blocos', scope.blocos);
        Lockr.set('ordem', scope.ordem);
        return id;
    }

    static atualizar(id, bloco){
        if(bloco)
            scope.blocos[id] = bloco;   
        Lockr.set('blocos', scope.blocos);
    }

    static deletar(id) {
        delete scope.blocos[id];
        scope.ordem.splice(scope.ordem.indexOf(id),1);
        scope.countBlocos--;
        Lockr.set('blocos', scope.blocos);
        Lockr.set('ordem', scope.ordem);
    }

    static limpar() {
        Lockr.set('blocos', {});
        Lockr.set('ordem',  []);
        scope.blocos      = {};
        scope.ordem       = [];
        scope.countBlocos = 0;
    }

    constructor(propriedades){
        var scope = Bloco.getScope();
        
        this.countRegistros = 0;
        this.saldo          = 0;
        this.nome           = propriedades.nome || false;
        this.registros      = {};

        // Validação dos dados do bloco;
        if (typeof this.nome != 'string') 
            throw scope.lang.erros.nomeInvalido;       
        
        if (propriedades.registros) {
            for (var r in propriedades.registros)
                this.inserirRegistro(propriedades.registros[r]);
        }
    }

    inserirRegistro(registro, data){

        if (data) {
            registro.dt = data.toLocaleString(scope.local,{
                day: '2-digit',
                month: '2-digit',
                year: 'numeric'
            });

            registro.hr = data.toLocaleString(scope.local,{
                hour: '2-digit',
                minute: '2-digit'
            });
        }

        var last = Number(Object.keys(this.registros).pop());
        var id = isNaN(last) ? 0 : last + 1;
        this.registros[id] = registro;
        this.countRegistros++;
        return this.countRegistros - 1;
    }

    deletarRegistro(idRegistro){
        delete this.registros[idRegistro];
        this.countRegistros--;
    }

    atualizarRegistro(idRegistro, registro){
        if(typeof this.registros[idRegistro] != 'object')
            throw 'tentando atualizar registro inexistente';
        this.registros[idRegistro] = registro;
    }

    limparRegistros(){
        this.countRegistros = 0;
        this.registros      = {};
        this.saldo          = 0;
    }

}

window.Bloco = Bloco;
