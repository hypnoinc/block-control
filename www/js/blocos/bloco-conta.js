

class BlocoConta extends Bloco{


	constructor(propriedades, saldoInicial){

        super(propriedades);
        var scope = Bloco.getScope();
		
        this.tipo   = 1;

        // Não ta aceitando Zero
        //if (typeof saldo != 'number') 
        //    throw scope.lang.erros.saldoInvalido;

        if (saldoInicial) {
            //insert do primeiro registro com o valor inicial do bloco
            this.inserirRegistro({
                label: scope.lang.valorInicial,
                valor: saldoInicial
            });    
        }
		

	}

    inserirRegistro(registro){
        if (registro.valor == 0) 
            throw scope.lang.erros.valorZero;   
        super.inserirRegistro(registro, new Date());
        this.saldo += registro.valor;

    }

    deletarRegistro(idRegistro){
        var valorDiscontar = this.registros[idRegistro].valor;
        super.deletarRegistro(idRegistro);
        this.saldo -= valorDiscontar;
    }

}
window.BlocoConta = BlocoConta;
