

class BlocoMe2us extends Bloco{


	constructor(propriedades, iniciar){

        super(propriedades);
        var scope = Bloco.getScope();
		
        this.tipo = 8;

        if (iniciar) {
            //Insere as informações nescessarias para converter as unidades
            super.inserirRegistro({
                label1: scope.lang.unidades.metros,
                label2: scope.lang.unidades.feets,
                valor: 3.28
            });
            super.inserirRegistro({
                label1: scope.lang.unidades.kilometros,
                label2: scope.lang.unidades.miles,
                valor: 0.621
            });
            super.inserirRegistro({
                label1: scope.lang.unidades.gramas,
                label2: scope.lang.unidades.ounces,
                valor: 0.03527
            });
            super.inserirRegistro({
                label1: scope.lang.unidades.kilogramas,
                label2: scope.lang.unidades.pounds,
                valor: 2.2046
            });
            super.inserirRegistro({
                label1: scope.lang.unidades.litros,
                label2: scope.lang.unidades.gallons,
                valor: 0.26412
            });
            super.inserirRegistro({
                label1: scope.lang.unidades.kilometrosphora,
                label2: scope.lang.unidades.milesphora,
                valor: 0.621
            });
        }

	}

}
window.BlocoMe2us = BlocoMe2us;
