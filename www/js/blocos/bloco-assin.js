

class BlocoAssin extends Bloco{


	constructor(propriedades){

        super(propriedades);
        var scope = Bloco.getScope();
		
        this.tipo   = 4;

	}

    inserirRegistro(registro){
        if(registro.valor == '' || registro.valor == 'undefined')
            registro.valor = 0;
        super.inserirRegistro(registro, new Date());
        this.saldo += registro.valor;
    }

    pagar(idRegistro, valor){
        if(valor == '' || valor == 'undefined')
            throw scope.lang.erros.valorZero;
        this.registros[idRegistro].valor += valor;
        this.saldo += valor;
    }

    cotar(valor){
        if(valor == '' || valor == 'undefined')
            throw scope.lang.erros.valorZero;
        var cota = valor / this.countRegistros;    
        for (var i in this.registros){
            var r = this.registros[i];
            r.valor -= cota;
            this.atualizarRegistro(i, r);
            this.saldo -= cota;
        }
    }
    
    deletarRegistro(idRegistro){
        var valorDiscontar = this.registros[idRegistro].valor;
        super.deletarRegistro(idRegistro);
        this.saldo -= valorDiscontar;
    }

}
window.BlocoAssin = BlocoAssin;
