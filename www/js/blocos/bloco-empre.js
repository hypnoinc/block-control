

class BlocoEmpre extends Bloco{


	constructor(propriedades){

        super(propriedades);
        var scope = Bloco.getScope();
		
        this.tipo   = 5;

	}

    pagar(idRegistro, valor){
        if(valor == '' || valor == 'undefined')
            throw scope.lang.erros.valorZero;
        this.registros[idRegistro].valor += valor;
        this.saldo += valor;
    }

    receber(idRegistro, valor){
        if(valor == '' || valor == 'undefined')
            throw scope.lang.erros.valorZero;
        this.registros[idRegistro].valor -= valor;
        this.saldo -= valor;
    }

    inserirRegistro(registro){

        super.inserirRegistro(registro, new Date());
        this.saldo += registro.valor;

    }

    deletarRegistro(idRegistro){
        var valorDiscontar = this.registros[idRegistro].valor;
        super.deletarRegistro(idRegistro);
        this.saldo -= valorDiscontar;
    }

}
window.BlocoEmpre = BlocoEmpre;
