ons.platform.select('android');
var module = angular.module('my-app', ['onsen']);

module.controller('AppController', function($scope) {

    $scope.versao          = '0.0.2-b';
    $scope.bloco           = {};
    $scope.registro        = {label: '', valor: ''};
    $scope.countBlocos     = 0;
    $scope.blocos          = {};
    $scope.filtroCategoria = 'controle';
    $scope.id              = 0;
    $scope.A               = '';
    $scope.B               = '';

    $scope.iniciar = function() {
        if (!first($scope)) 
            $scope.defineIdioma(Lockr.get('lang'));

        Bloco.setScope($scope);        
        $scope.blocos = Bloco.buscar();
        $scope.ordem  = Lockr.get('ordem');
        $scope.countBlocos = Object.keys($scope.blocos).length;
    };

    $scope.mostraDetalhes = function(bloco) {
        $scope.icone  = bloco.icone;
        $scope.descr  = bloco.descr;
        $scope.titulo = bloco.nome;
        dialog.show()
    };
    
    $scope.novoBloco = function(){
        $scope.bloco = {};
    }

    $scope.setId = function (id){
        $scope.id = id;
    }

    $scope.copiar = function (text){
        cordova.plugins.clipboard.copy(text);
        ons.notification.alert($scope.lang.copiado);
    }
    
    $scope.setFator = function (fator){
        $scope.fator = fator;
    }

    $scope.abrirModal = function (modal,idFoco){
        window[modal].show(); 
        if (idFoco)
            document.getElementById(idFoco).focus();
    }
    
    $scope.salvarBloco = function(classe){
        
        try{
            var b = new window[classe]($scope.bloco, $scope.bloco.saldo || false);
            Bloco.inserir( b );
        }
        catch(msg){
            ons.notification.alert(msg);
            return;
        }

        //redirecionamento para home
        myNavigator.resetToPage('views/home.html');
    }

    $scope.renomearBloco = function(idBloco){
        $scope.bloco.ordem = $scope.countBlocos + 1;
        try{
            Bloco.atualizar( idBloco );
        }
        catch(msg){
            ons.notification.alert(msg);
            return;
        }
        modalRenomear.hide();
    }

    $scope.deletarBloco = async function(idBloco){
        var retorno = await ons.notification.confirm(
            $scope.lang.confirmMsg.deletarBloco,
            { buttonLabels: [$scope.lang.btCancelar, $scope.lang.btConfirmar] }
        );
        if(retorno){
            
            try{
                Bloco.deletar( idBloco );
            }
            catch(msg){
                ons.notification.alert(msg);
                return;
            }
            $scope.countBlocos = Object.keys($scope.blocos).length;
         
            //Atualiza scope no app (async function)
            $scope.$digest();

            //redirecionamento para home
            myNavigator.resetToPage('views/home.html');
        }
    }

    $scope.deletarTodosBlocos = async function(){
        var retorno = await ons.notification.confirm(
            $scope.lang.confirmMsg.deletarTodosRegistros,
            { buttonLabels: [$scope.lang.btCancelar, $scope.lang.btConfirmar] }
        );
        if(retorno){
            
            try{
                Bloco.limpar();
            }
            catch(msg){
                ons.notification.alert(msg);
                return;
            }
            $scope.countBlocos = Object.keys($scope.blocos).length;
         
            //Atualiza scope no app (async function)
            $scope.$digest();

            //redirecionamento para home
            myNavigator.resetToPage('views/home.html');
        }
    }

    $scope.deletarRegistro = async function(idBloco, idRegistro){
        var retorno = await ons.notification.confirm(
            $scope.lang.confirmMsg.deletarBloco,
            { buttonLabels: [$scope.lang.btCancelar, $scope.lang.btConfirmar] }
        );
        if(retorno){
            $scope.blocos[idBloco].deletarRegistro(idRegistro);
            Bloco.atualizar();
            $scope.$digest();
        }
    }

    $scope.limparRegistros = async function(idBloco){
        var retorno = await ons.notification.confirm(
            $scope.lang.confirmMsg.deletarTodosBlocos,
            { buttonLabels: [$scope.lang.btCancelar, $scope.lang.btConfirmar] }
        );
        if(retorno){
            $scope.blocos[idBloco].limparRegistros();
            Bloco.atualizar();
            $scope.$digest();
            ons.notification.alert($scope.confirmMsg.registrosDeletados);
        }
    }

    $scope.baixarOrdem = function(key) {
        var a = $scope.ordem[key];
        $scope.ordem[key] = $scope.ordem[key+1];
        $scope.ordem[key+1] = a;
        Lockr.set('ordem', $scope.ordem);
    }

    $scope.sobeOrdem = function(key) {
        var a = $scope.ordem[key];
        $scope.ordem[key] = $scope.ordem[key-1];
        $scope.ordem[key-1] = a;
        Lockr.set('ordem', $scope.ordem);
    }

    $scope.defineIdioma = function(idioma) {
        var idiomaValido = typeof getDados[idioma] == 'function';
        idioma = idiomaValido ? idioma : 'en-US';
        getDados[idioma]($scope);
        Lockr.set('lang', idioma);
        $scope.local = idioma;
    }
    
    $scope.lancarRegistro = function(idBloco, sacar) {
        var registro = {valor : $scope.registro.valor * (sacar ? -1 : 1),
                        label : $scope.registro.label || (sacar ? $scope.lang.saque : $scope.lang.deposito),
                        dt: '',
                        hr: ''};
        try {
            $scope.blocos[idBloco].inserirRegistro(registro);
            Bloco.atualizar();
        } catch(msg) {
            ons.notification.alert(msg);
            return;
        }
        modalLancar.hide();
        $scope.registro.valor = '';
        $scope.registro.label = '';
    }

    $scope.pagar = function(idBloco, idRegistro) {
        try {
            $scope.blocos[idBloco].pagar(idRegistro, $scope.registro.valor);
            Bloco.atualizar();
        } catch(msg) {
            ons.notification.alert(msg);
            return;
        }
        modalEditar.hide(); 
        $scope.registro.valor = '';
    }

    $scope.receber = function(idBloco, idRegistro) {
        try {
            $scope.blocos[idBloco].receber(idRegistro, $scope.registro.valor);
            Bloco.atualizar();
        } catch(msg) {
            ons.notification.alert(msg);
            return;
        }
        modalEditar.hide(); 
        $scope.registro.valor = '';
    }

    $scope.cotarAssin = function(idBloco) {
        try {
            $scope.blocos[idBloco].cotar($scope.registro.valor);
            Bloco.atualizar();
        } catch(msg) {
            ons.notification.alert(msg);
            return;
        }
        modalCotar.hide(); 
        $scope.registro.valor = '';
    }

    $scope.editarRegistro = function(idBloco, idRegistro) {
        try {
            $scope.blocos[idBloco].atualizarRegistro(idRegistro, $scope.registro);
            Bloco.atualizar();
        } catch(msg) {
            ons.notification.alert(msg);
            return;
        }

        modalEditar.hide();    
        $scope.registro.valor = '';
        $scope.registro.label = '';
    }
    
});

document.addEventListener("deviceready", iniAdMob, false);
function iniAdMob(){
    //ADmob
    admob.banner.config({
        id: 'ca-app-pub-9305700364188858/2184975179',
    });
    admob.banner.prepare();
    admob.banner.show();  
}