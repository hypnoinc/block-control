# Bloco Control


###### Postuguês
### Descrição 

É um aplicativo para android com ferramentas de controle financeiro e outras ferramentas utéis. 
O usuario seleciona as ferramentas que ele que utilizar e elas ficam disponiveis na pagina incial do app.

###### English
### Description

It is an android application with financial control tools and other useful tools.
The user selects the tools he uses and they are available in the initial page of the app.

###### Postuguês
### Técnologias Utilizadas 

Este é um aplicativo hibrido baseado em CORDOVA, puramente HTML5, CSS e Javascript. Incluindo tbm dois Frameworks AngularJS e Onsen.

###### English
### Used Technologies

This is a hybrid application based on CORDOVA, purely HTML5, CSS and Javascript. Including two frameworks AngularJS and Onsen.